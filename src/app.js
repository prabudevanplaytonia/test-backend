import express from 'express'

import { login, users } from './components/user/userController.js'

const app = express()
const port = 3000

app.use(express.json())

app.post('/login', login)

app.get('/users', users)

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})
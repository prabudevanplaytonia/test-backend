const userList = [
    {
        username: 'spd',
        password: 'spd'
    }
]


export const login = async (req, res) => {
	try {

        console.log(req.body)
        const user = userList.find(user => user.username === req.body.username)

        if (!user) return res.status(400).json('username not found')

        if (user.password === req.body.password) return res.json('login successful')

        else return res.status(400).json('incorrect password')
        
	} catch (error) {
		console.log(error)
		return res.status(400).json(error)
	}
}

export const users = async (req, res) => {
	try {
        return res.json(userList)    
	} catch (error) {
		console.log(error)
		return res.status(400).json(error)
	}
}